Coding challenge to implement a simple REST API with Django REST Framework to synchronize remote mobile devices executing Tasks. Full description hosted by [Ernie-App](http://testdjango.ernie-app.com).


### How to run
There are two options:

1. `docker-compose build` and then `docker-compose up`. The api is now available on http://localhost
2. `pip install pipenv && pipenv install` and then `pipenv run python manage.py runserver`

### How to use

At first, Customer is expected to create an account using the following endpoint:
```api/v1/rest-auth/registration/```

Next, Customer should create tasks using `api/v1/tasks/` endpoint.

Next, Customer should add one or more devices using `api/v1/devices/` endpoint.

An available task is started using Device endpoint, like, `api/v1/devices/6cb412e4-68b0-41d4-b3c2-dde438117476/start/`.
If there are tasks that are not taken by other devices, one task is chosen and assigned to the Device.

The task is finished by the device that started it by accessing `api/v1/devices/6cb412e4-68b0-41d4-b3c2-dde438117476/complete/result=Success`

or by reporting a reason why the task couldn't be completed `api/v1/devices/6cb412e4-68b0-41d4-b3c2-dde438117476/complete/error=something wrong`
