from rest_framework.routers import SimpleRouter
from .views import TaskViewSet, DeviceViewSet


router = SimpleRouter()
router.register('tasks', TaskViewSet, basename='tasks')
router.register('devices', DeviceViewSet, basename='devices')
urlpatterns = router.urls
