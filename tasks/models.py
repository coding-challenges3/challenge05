from django.contrib.auth.models import AbstractUser
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
import uuid
from datetime import datetime, timezone
from django.conf import settings


class Customer(AbstractUser):
    pass


class Task(models.Model):
    customer = models.ForeignKey(
        Customer,
        on_delete=models.CASCADE,
        related_name='tasks',
        null=False)
    name = models.CharField(max_length=20, null=True)

    def _get_state(self):
        if self.result and not self.error:
            return 0  # 0 = Successfully completed
        if self.error:
            return 1  # 1 = not started / needs retry
        try:
            s = TasksStarted.objects.get(task_id=self.id)
            now = datetime.now(timezone.utc)
            d = now - s.started
            if d.seconds <= settings.TASK_TIMEOUT:
                return 2  # 2 = running
            else:
                return 3  # 3 = expired
        except ObjectDoesNotExist:
            return 1  # 1 = not started / needs retry

    state = property(_get_state)

    # The non-error result returned by Device
    result = models.CharField(max_length=200, blank=True, null=True)

    # Device may report an error and it should be stored here
    error = models.CharField(max_length=200, blank=True, null=True)

    def __str__(self):
        if self.state == 0:
            s = "completed"
        elif self.state == 1:
            s = "not running"
        elif self.state == 2:
            s = "running"
        elif self.state == 3:
            s = "expired"
        return f"{self.pk} - {self.name} - State: {s}"


class Device(models.Model):
    customer = models.ForeignKey(
        Customer,
        on_delete=models.CASCADE,
        related_name='devices',
        null=False)

    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False)

    name = models.CharField(max_length=20, null=True)

    task = models.ManyToManyField(Task, through='TasksStarted')

    def __str__(self):
        if self.name:
            return f"{self.name}"
        else:
            return f"{self.id}"


class TasksStarted(models.Model):
    task = models.OneToOneField(
        Task,
        on_delete=models.CASCADE,
        related_name='task')

    device = models.OneToOneField(
        Device,
        on_delete=models.CASCADE,
        related_name='device')

    started = models.DateTimeField(auto_now=True)

    def __str__(self):
        now = datetime.now(timezone.utc)
        d = now - self.started

        if d.seconds <= settings.TASK_TIMEOUT:
            return (f"{self.task.name} running on "
                    f"{self.device} for {d.seconds} seconds")
        else:
            return "Expired task"
