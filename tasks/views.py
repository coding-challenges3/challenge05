from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from datetime import datetime, timezone
from .models import Task, Device, TasksStarted
from .permissions import IsOwner
from .serializers import (
    TaskSerializer, DeviceSerializer, CompletedTaskSerializer)


class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    permission_classes = (IsAuthenticated, IsOwner,)

    def perform_create(self, serializer):
        serializer.save(customer=self.request.user)

    def get_queryset(self, *args, **kwargs):
        return Task.objects.filter(customer=self.request.user)


class DeviceViewSet(viewsets.ModelViewSet):
    queryset = Device.objects.all()
    serializer_class = DeviceSerializer
    permission_classes = (IsAuthenticated, IsOwner,)

    def perform_create(self, serializer):
        serializer.save(customer=self.request.user)

    def get_queryset(self, *args, **kwargs):
        return Device.objects.filter(customer=self.request.user)

    @action(detail=True)
    def start(self, request, *args, **kwargs):
        d = self.get_object()

        # first - maintenance of expired tasks
        tasksstarted = TasksStarted.objects.all()
        if len(tasksstarted) > 0:
            for t in tasksstarted:
                if t.task.state == 3 and t.task.customer == request.user:
                    t1 = t
                    t1.delete()
                if t.task.state == 2 and t.device == d:
                    # restart the task
                    t.started = datetime.now(timezone.utc)
                    t.save()
                    return Response({
                        "task": t.task.name,
                        "status": "started",
                        "started_at": t.started
                        })

        # second - check if already is running a task
        tasks = Task.objects.filter(customer=request.user)
        for t in tasks:
            if t.state == 1:
                d.task.add(t)
                return Response({"task": t.name})
        else:
            return Response({"task": ''})

    @action(detail=True, methods=['post', 'get'])
    def complete(self, request, *args, **kwargs):
        d = self.get_object()
        serializer = CompletedTaskSerializer(data=request.GET)
        if serializer.is_valid():
            tasksstarted = TasksStarted.objects.filter(device=d)
            if len(tasksstarted) > 0:
                task = tasksstarted[0].task
                if serializer.data['error']:
                    task.error = serializer.data['error']
                    task.save()
                    tasksstarted[0].delete()
                    return Response({
                        "task": task.name,
                        "status": "failed",
                        "error": serializer.data['result']
                        })
                elif serializer.data['result']:
                    task.error = None
                    task.result = serializer.data['result']
                    task.save()
                    tasksstarted[0].delete()
                    return Response({
                        "task": task.name,
                        "status": "completed",
                        "result": serializer.data['result']
                        })
                else:
                    return Response({"status": "invalid"})
            else:
                return Response({"status": "invalid"})
        else:
            return Response({"status": "invalid"})
