from rest_framework import serializers
from tasks.models import Device, Task


class TaskSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Task
        fields = ('id', 'name', 'state')


class CompletedTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ('id', 'result', 'error')


class DeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Device
        fields = ('name', 'id', 'task')
