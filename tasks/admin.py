from django.contrib import admin

# Register your models here.

from .models import (
    Customer,
    Device,
    Task,
    TasksStarted,
    )

admin.site.register(Customer)
admin.site.register(Device)
admin.site.register(Task)
admin.site.register(TasksStarted)
